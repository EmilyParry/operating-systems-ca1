#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define LEN 256


//writeToFile(void);

int main()
{
  unsigned int  offset_mask = 0x00FF;
  unsigned int address = 0xFFFE;// 16 bit address first 8 bits = frame. last 8 = offset
  unsigned int offset = address & offset_mask;

  unsigned vpn = address >> 8; // 8 bits from vpn
  printf("Address: 0x%X\nOffsetmask: 0x%X\nOffset: 0x%X\nVpn: 0x%X\n",address,offset_mask,offset,vpn);
  char *memory = malloc(65536); // memory allocation. its 65536 as its 2^16. * - pointer.
	
  srand(time(0));
  int random = 2048 + rand()%18433; // https://stackoverflow.com/questions/43663797/c-how-to-generate-random-numbers-between-20-and-30  

	char *pagetable;
	pagetable = (char *) malloc(512);// 256*2

	 FILE *w2f; // write to file
   w2f = fopen("physical_memory.txt","w");
   
   fprintf(w2f, "Address | Frame| Content\n");
   fprintf(w2f, "--------|------|-------- \n");
   int i=0;
   int frameNum =0;
   int addressNum=0;
   int count =0;
   char contentChar;

   for(i=0; i < 65535;i++)
   { 
	addressNum = count;
	contentChar= (char)rand() %26+65; // stackoverflow.com/questions/3570673/use-of-rand-function-to-generate-ascii-values	
        fprintf(w2f,"0x%X | %d | %c\n", addressNum,frameNum,contentChar);
	if(count>0 && count %256 ==0) // if the count is dividable by 256 and the remainder is 0 add 1 onto the frame.
	{
		frameNum=frameNum +1;
	}
	count=count +1;
   }  
   
   fclose(w2f);
  return 0;

}
/*
void writeToFile()
{
   printf("helloworld");
   
   File * w2f; // write to file
   w2f = fopen("\gitCa1\\physical_memory.txt","w");
   
   fprintf(w2f, "hello world\n");
   fclose(w2f);
}
*/
